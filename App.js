import { StatusBar } from "expo-status-bar";
import Components from "./src/components/components";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import SegundaTela from "./src/segundaTela";
import TelaInicial from "./src/telaInicial";
import Menu from "./src/components/menu";
import TerceiraTela from "./src/terceiraTela";
import QuartaTela from "./src/quartaTela";
import QuintaTela from "./src/quintaTela";


const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="TelaInicial" component={TelaInicial} />
        <Stack.Screen name="SegundaTela" component={SegundaTela} />
        <Stack.Screen name="TerceiraTela" component={TerceiraTela} />
        <Stack.Screen name="QuartaTela" component={QuartaTela} />
        <Stack.Screen name="QuintaTela" component={QuintaTela} />
        

      </Stack.Navigator>
    </NavigationContainer>
  );
}